// @flow

import path from 'path';
import { confirmFolderExists, log, flatten, get, post, writeFile } from './utility';

// Define urls
const urls = {
  pages: itemId => `http://www.etera.ee/api/item/${itemId}/pages`,
  meta: itemId => `http://www.etera.ee/api/item/${itemId}/meta`,
  rects: (itemId, pageId) => `http://www.etera.ee/api/item/${itemId}/clip/${pageId}/find`
};

// Define output folder
const outputFolder = path.resolve(__dirname, '..', 'output');
confirmFolderExists(outputFolder);

// Define worker functions
const getPages = itemId => get(urls.pages(itemId));
const getRects = (itemId, pageId) => get(urls.rects(itemId, pageId));
const getText = (itemId, pageId, x, y) => post(urls.rects(itemId, pageId), { rect: { x, y } });

// Define a function to run the process
const getTexts = (itemId: number) => getPages(itemId)
  .then(pages => Promise.all(pages.pages.map(page => getRects(itemId, page.attributes.id))))
  .then(flatten)
  .then(rects => rects.filter(rect => rect.type === 'text'))
  .then(rects =>
    Promise.all(rects.map(rect => getText(itemId, rect.pageId, rect.rect.x, rect.rect.y))))
  .then(flatten)
  .then(rects => rects.map(rect => rect.text))
  .then(texts => writeFile(path.resolve(__dirname, '..', 'output', `output.${itemId}.json`), JSON.stringify(texts, null, 2)))
  .catch(log);

// Export, just in case someone wants to import this somewhere
export default getTexts;


// Run this manually since we havent integrated it to anything yet
getTexts(23213);
