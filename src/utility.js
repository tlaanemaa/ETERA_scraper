// @flow

import request from 'request';
import fs from 'fs';
import createLimiter from 'limit-async';

const requestLimit = createLimiter(1);

/* eslint-disable no-console */
export const log = <T>(...args: Array<T>) => console.log(...args);
/* eslint-enable no-console */

export const confirmFolderExists = (path: string) => {
  try {
    fs.accessSync(path, fs.constants.W_OK);
    return true;
  } catch (e) {
    fs.mkdirSync(path);
    return false;
  }
};

export const flatten = <T>(arr: Array<Array<T>>) => [].concat(...arr);

export const get = requestLimit(url => new Promise((resolve, reject) => {
  log(` >> Making GET request to ${url}`);
  request(
    {
      url,
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36' },
      json: true
    },
    (err, result, body) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(body);
    }
  );
}));

export const post = requestLimit((url, data) => new Promise((resolve, reject) => {
  log(` >> Making POST request to ${url} with data ${JSON.stringify(data)}`);
  request(
    {
      url,
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36' },
      method: 'post',
      body: data,
      json: true
    },
    (err, result, body) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(body);
    }
  );
}));

export const writeFile = <T>(filename: string, data: T): Promise<T> =>
  new Promise((resolve, reject) => {
    const text = typeof data === 'string' ? data : JSON.stringify(data);
    fs.writeFile(filename, text, (err) => {
      if (err) {
        reject(err);
        return;
      }
      log(`\n >> Saved output to ${filename}`);
      resolve(data);
    });
  });
