# ETERA Scraper
###### How to Use
1. Make sure you have [node.js](https://nodejs.org/en/) installed
2. Clone this repo
3. Navigate into it in terminal and run `npm install`
4. Run `npm start` to run the scraper

This thing has no UI yet so you'll just have to change the index.js file in src folder to make it download different books. At the end of the file there is a block that looks like this:
```js
// Run this manually since we havent integrated it to anything yet
getTexts(23213);
```
The number provided to `getTexts` is ETERA id of the book you want to download.
To download multiple books, create an array with those book ids and [map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) the `getTexts` function over it like so:
```js
[1, 2, 3, 4, 5].map(getTexts);
```
ETERA book id can be found in that book's URL on ETERA website.
For example, the id for ["Arvutuslingvistikalt inimesele"](http://www.etera.ee/zoom/22476/view) is 22476.

Output will be saved under output folder in the root directory of this project.

If for some reason it seems too slow then that's because its limited to 1 concurrent request at a time. If you'd like you can increase that and thus make it noticeably faster. The concurrent request limit is defined in the utility.js file in src folder at the top like this:
```js
const requestLimit = createLimiter(1);
```
Beware however that more concurrent requests will put more stress on ETERA server and might get you banned or something.
